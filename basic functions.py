#!/usr/bin/env python
# coding: utf-8

# In[15]:


x = lambda a : a + 10
print(x(5))


# In[16]:


x = lambda a: a+10
print (x(5))


# In[17]:


x = lambda a,b : a*b
print(x(9,8))


# In[18]:


D = {'NAVEEN':'SAMSUNG','ISHA':'ONEPLUS'}


# In[19]:


print(D)


# In[20]:


D.keys()


# In[21]:


D.keys()


# In[12]:


D.values()


# In[13]:


Dict = {1:'Neha', 2:'Megha', 3:'Varnika', 4:'Rishu', 5:'Anshuk'}
print(Dict)


# In[23]:


Dict.keys()


# In[24]:


Dict.values()


# In[28]:


name = 'neha,' *20
print(name)


# In[29]:


x = 7
x %= 3
print(x)


# In[30]:


7%3


# In[31]:


x = 5
x+= 10
print(x)


# In[32]:


name = 'youtube'
print(name)


# In[33]:


name + ' rocks'


# In[34]:


x = (1,4)
y = (2,3)


# In[49]:


name = 'youtube'
len(name)


# In[50]:


a = lambda x : x*x
print(a(5))


# In[51]:


x = lambda a,b : a*b
print(x(9,8))


# In[52]:


def urp(a):
    return a*a
print(urp(10))


# In[53]:


def my_function(fname):
  print(fname + "emli")


# In[54]:


def my_function(fname):
  print(fname + " Refsnes")


# In[56]:


import math


# In[57]:


x = math.ceil(2.9)


# In[58]:


import math


# In[59]:


x


# In[64]:


l1 = [1,2,3,4,5]
l2 = [5,4,3,2,1]
if l1 > l2:
    print("the l1 is greater")
else :
    print("l2 is greater")
     


# In[65]:


value = 123
print(value, 'is', 'even' if value % 2 == 0 else 'odd')


# In[72]:


def find_Evenodd(num):
    number=(num/2)*2
    if(number==num):
        print(num, "is a even number");
    else:
        print(num, "is a odd number");


# In[73]:


print(find_Evenodd(943))


# In[77]:


def find_odd_even(num):
    number = (num/2)*2
    if(number == num) :
        print(num,"is a odd number");
    else:
        print(num,"is a even number");


# In[78]:


thisset = {"apple", "banana", "cherry"}
print(thisset)


# In[79]:


abc = {1,2,3}
print(abc)


# In[80]:


bc = {9,11,2,78,78}
print(bc)


# In[81]:


thisset = {"apple", "banana", "cherry"}

for a in thisset:
  print(a)


# In[82]:


thisset = {"apple", "banana", "cherry"}

print("banana" in thisset)


# In[83]:


thisset = {"apple", "banana", "cherry"}

thisset.add("orange")

print(thisset)

